import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CopangOne from '~/components/copang-one'
Vue.component('CopangOne', CopangOne)
